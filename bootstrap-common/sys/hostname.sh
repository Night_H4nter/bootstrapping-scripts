#!/usr/bin/env bash
set -e


#set hostname
if [ $# -eq 0 ]
  then
    HOSTNAME="nhdpc"
else
    HOSTNAME="$1"
fi

echo "$HOSTNAME" > /etc/hostname
