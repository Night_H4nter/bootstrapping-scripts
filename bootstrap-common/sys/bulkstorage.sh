#!/usr/bin/env bash
set -e


#add bulk storage and set its owner
if [ -d "/data" ]
then
    mkdir /data
    mount -o nosuid,noexec,nodev /dev/datavg/lvol0 /data
    echo "/dev/datavg/lvol0       /data   ext4    nosuid,noexec,nodev     0 0" | tee -a /etc/fstab
    chown daniel:daniel -R /data
else
    echo '/data exists'
    exit 1
fi
