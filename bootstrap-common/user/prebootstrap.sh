#!/usr/bin/env bash


echo "
Preinstall:
  - Import ssh keys
  - Install git
  - Install KwinDE icon theme (it doesn't work if fetched from gitlab for some reason)
" | tee "$HOME/preinstall"
