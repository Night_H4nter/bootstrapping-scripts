#!/usr/bin/env bash
set -e


#install software that is meant to come as flatpak
flatpak install -y --noninteractive com.microsoft.Edge com.github.tchx84.Flatseal org.telegram.desktop org.telegram.desktop.webview com.discordapp.Discord org.mozilla.firefox com.google.Chrome org.gtk.Gtk3theme.Adwaita-dark com.usebottles.bottles
