CONFDIR = "$(CURDIR)/confstorage"
#text colors
RED = $'\e[1;31m
YEL=$'\e[1;33m
END = $'\e[0m


initialize:
	mkdir -p "$(CONFDIR)"
	git clone git@gitlab.com:Night_H4nter/nvimconfig.git "$(CONFDIR)/nvimconfig" || true


nvim:
	@printf "$(YEL)Note: Neovim will start and exit one or more time during setup.$(END)"
	@printf "\n"
	@sleep 3
	
	curl -fLo "$(HOME)/.local/share/nvim/site/autoload/plug.vim" --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	cd "$(CONFDIR)/nvimconfig"; stow -v --target=$(HOME) nvim
	nvim "+PlugInstall" +qall
	#a crutch, this must be addressed somehow
	sed -Ei 's|PyYAML.*|PyYAML|g' "$(HOME)/.local/share/nvim/plugins/coq_nvim/requirements.txt"
	# nvim "+call FinishBootstrapping()" #"+q"
	# nvim +COQdeps
	
	@printf "\n$(YEL)Installation process complete.$(END)"
	@printf "\n$(RED)Note: start Neovim, run :call FinishBootstrapping(), quit Neovim then start it again.$(END)"
	@printf "\n$(RED)Ignore if CoQ complains before you complete these steps.$(END)"
	@printf "\n$(YEL)Yes, this is a crutch, but CoQ is really annoying.$(END)"
	@printf "\n"
