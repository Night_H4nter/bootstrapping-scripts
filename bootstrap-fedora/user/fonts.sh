#!/usr/bin/env bash
set -e


#fetch and install fonts
fc-cache -frv
git clone git@gitlab.com:Night_H4nter/fonts.git "$HOME/.local/share/fonts"
fc-cache -frv
gsettings set org.gnome.desktop.interface font-name 'Segoe UI Regular 11'
gsettings set org.gnome.desktop.interface document-font-name 'Segoe UI Regular 11'
gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Segoe UI Regular 11'
gsettings set org.gnome.desktop.interface monospace-font-name 'Hack Nerd Font Mono 11'
