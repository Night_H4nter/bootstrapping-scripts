#!/usr/bin/env bash


echo "
Gnome extensions and their settings (I don't know if there's a way to install them automatically):

  - Hot Edge:
	Activation Pressure  			130
	
  - Just Perfection:
	Always Show Workspace Switcher  	true
	Startup status 				desktop
	Dash Icon Size 				32px
	Workspace Switcher Size 		18%
	Animation 				Fastest
	
  - Vitals:
	Monitor:
		Memory 				Usage
		Processor 			Average
		Storage 			Free
	Use Fixed Widths 			True
	
  - Sound Input & Output Device Chooser
  	Integrate selector with slider 		True
  	Extend Volume Menu to fit device names 	False
  	
  - Panel scroll
  
  - Status Area Horizontal Spacing
  	~1/4 of the slider

  - AppIndicator and KStatusNotifier Support
  	Opacity 				255
	Desaturation 				1

  - Tiling Assistant

Other stuff to do:
  - Discord:
	Hide muted channels
	Add hotkeys
  
  - Telegram:
  	Enable night mode
	Enable native notifications
	Enable native title bar
	Enable hardware acceleration
	Set scale to 125%
	Move archive to main menu
	Set download path maybe?
  
  - Adjust monitor settings
" | tee "$HOME/postinstall"

