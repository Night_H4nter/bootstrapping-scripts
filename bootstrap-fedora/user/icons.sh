#!/usr/bin/env bash
set -e


#download icons pack and enable it
mkdir "$HOME/.local/share/icons"
git clone https://gitlab.com/Night_H4nter/kwinde "$HOME/.local/share/icons/KwinDE"
gsettings set org.gnome.desktop.interface icon-theme KwinDE
