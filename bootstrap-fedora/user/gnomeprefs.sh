#!/usr/bin/env bash
set -e


#set dark theme for modern and legacy apps
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
gsettings set org.gnome.desktop.interface gtk-theme Adwaita-dark
#allow audio over-amplification
gsettings set org.gnome.desktop.sound allow-volume-above-100-percent true
#disable event sounds
gsettings set org.gnome.desktop.sound event-sounds false
#set default applications
xdg-settings set default-web-browser com.microsoft.Edge.desktop
#set aa and hinting
gsettings set org.gnome.desktop.interface font-antialiasing rgba
gsettings set org.gnome.desktop.interface font-hinting full
#show weekday in clock
gsettings set org.gnome.desktop.interface clock-show-weekday true
#show week number in calendar widget
gsettings set org.gnome.desktop.calendar show-weekdate true
#set middle click to lower window
gsettings set org.gnome.desktop.wm.preferences action-middle-click-titlebar lower
#set focus on hover
gsettings set org.gnome.desktop.wm.preferences focus-mode mouse   # sloppy ?
#autoraise focused window
gsettings set org.gnome.desktop.wm.preferences auto-raise true
#disable the fucking switch monitor hotkey that i trigger like every other day
#breaking my monitor config
gsettings set org.gnome.mutter.keybindings switch-monitor "['XF86Display']"
