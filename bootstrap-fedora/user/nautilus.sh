#!/usr/bin/env bash
set -e


#set gtk bookmarks
mkdir -p "$HOME/.config/gtk-3.0"
echo "file:///home/daniel/based" | tee -a "$HOME/.config/gtk-3.0/bookmarks"
echo "file:///data" | tee -a "$HOME/.config/gtk-3.0/bookmarks"
echo "file:///data/isos" | tee -a "$HOME/.config/gtk-3.0/bookmarks"
#sort by mtime by default
gsettings set org.gnome.nautilus.preferences default-sort-order mtime
