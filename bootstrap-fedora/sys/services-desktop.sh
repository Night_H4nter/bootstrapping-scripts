#!/usr/bin/env bash


#stop services unnecessary for desktop
DESKTOPSERVICES=("power-profiles-daemon.service" "switcheroo-control.service" "wpa_supplicant.service")
for SERVICE in ${DESKTOPSERVICES[@]}
do
    systemctl disable --now "$SERVICE"
done
