#!/usr/bin/env bash
set -e


#switch back to X by default TODO:disable wayland completely
sudo sed -i 's/#WaylandEnable=false/WaylandEnable=false\nDefaultSession=gnome-xorg.desktop/g' /etc/gdm/custom.conf
sudo systemctl restart gdm.service
