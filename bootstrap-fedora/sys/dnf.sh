#!/usr/bin/env bash


#update the system
dnf update -y

#add/remove repos
#rpmfusion, rpmfusion-nonfree
dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
#rpmfusion-tainted, rpmfusion-tainted-nonfree
dnf install -y rpmfusion-free-release-tainted rpmfusion-nonfree-release-tainted
#neovim nightly copr
dnf copr enable agriffis/neovim-nightly -y
#lazygit copr
dnf copr enable atim/lazygit -y
#i don't use pycharm, chrome is from flatpak
dnf config-manager --set-disabled google-chrome phracek-PyCharm

#install necessary software
#gnome-tweaks dconf-editor
#python3-virtualenv python3-Cython
#install everything else from repos
dnf install -y akmod-nvidia redshift stow zsh git make automake gcc openssh neovim python3-neovim tmux ansible lazygit vifm xbanish wireguard-tools golang-github-dreamacro-shadowsocks2 qpwgraph timeshift freerdp ffmpegthumbnailer gnome-epub-thumbnailer totem-video-thumbnailer gcc-c++
dnf groupinstall "Development Tools" "Development Libraries" -y
#remove unnecessary software
dnf remove -y firefox gnome-text-editor
