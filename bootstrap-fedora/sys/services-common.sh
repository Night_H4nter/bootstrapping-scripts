#!/usr/bin/env bash


#stop common unnecessary services
COMMONSERVICES=("fwupd.service" "gssproxy.service" "ModemManager.service" "pcscd.service" "rpc-statd-notify.service" "sssd-kcm.service" "mcelog.service" "pcscd.socket" "sssd-kcm.socket" "nvidia-powerd.service" "cups.service" "colord.service" "avahi-daemon.service" "avahi-daemon.socket")

for SERVICE in ${COMMONSERVICES[@]}
do
    systemctl disable --now "$SERVICE"
    systemctl mask "$SERVICE"
done
