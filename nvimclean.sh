#!/usr/bin/env bash


rm -rf "$HOME/.local/state/nvim"
rm -rf "$HOME/.config/coc"
rm -rf "$HOME/.local/share/nvim"

for TARGET in "$HOME/.config/nvim" "$HOME/.local/share/vim-lsp-settings" "$HOME/.config/coc"
do
    file "$TARGET" | grep -qF "symbolic link to" && rm -rf $TARGET \
        || echo "WARNING: $TARGET is not a not a symbolic link!"
done
